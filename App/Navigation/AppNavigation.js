import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Homepage from '../Screens/HomePage'
import DetailPage from '../Screens/DetailPage'


const Stack = createStackNavigator();

export default function AppNavigation() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={Homepage} options={{
                headerShown: false
            }} />
            <Stack.Screen name="DetailPage" component={DetailPage} options={{
                headerStyle: { alignItems: 'center', }
            }} />
        </Stack.Navigator>
    );
}