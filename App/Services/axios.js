import axios from 'axios';
import { BaseURL } from '../../App/Services/config';


const instance = axios.create({

    baseURL: BaseURL

});

instance.defaults.headers.post['Content-Type'] = 'application/json';

instance.interceptors.request.use(request => {
    return request;
}, error => {
    return Promise.reject(error);
});

instance.interceptors.response.use(response => {
    return response;
}, error => {
    return Promise.reject(error);
});


export default instance;