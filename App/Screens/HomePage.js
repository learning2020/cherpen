//This is an example of React Native 
//FlatList Pagination to Load More Data dynamically - Infinite List
import React, { Component } from 'react';
//import react in our code.

import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    FlatList,
    Platform,
    ActivityIndicator,
} from 'react-native';
//import all the components we are going to use.
import { api_key } from '../../App/Services/config';
import axios from '../../App/Services/axios'

import MovieList from '../Components/MovieList'

export default class HomePage extends Component {
    constructor() {
        super();
        this.onPressButton = this.onPressButton.bind(this);
        this.state = {
            loading: true,
            //Loading state used while loading the data for the first time
            serverData: [],
            //Data Source for the FlatList
            fetching_from_server: false,
            //Loading state used while loading more data
        };
        this.offset = 1;
        //Index of the offset to load from web API
    }
    onPressButton() {
        const { navigate } = this.props.navigation;

        navigate("DetailPage")

    }

    componentDidMount() {
        this.getGenreData();

    }


    getGenreData = async () => {




        const url = 'genre/movie/list?';
        axios
            .get(url, { params: { api_key } })
            .then(data => {
                console.log('geners data......................', data.data.genres);

                this.setState({

                    serverData: data.data.genres,
                    loading: false,
                    //display_name
                });

            })
            .catch(error => {
                console.log('error', error.message);
            });
    }


    loadMoreData = () => {
        //On click of Load More button We will call the web API again
        this.setState({ fetching_from_server: true }, () => {
            fetch('https://aboutreact.000webhostapp.com/demo/webservice/getpost.php?offset=' + this.offset)
                //Sending the currect offset with get request
                .then(response => response.json())
                .then(responseJson => {
                    //Successful response from the API Call 
                    this.offset = this.offset + 1;
                    //After the response increasing the offset for the next API call.
                    this.setState({
                        serverData: [...this.state.serverData, ...responseJson.results],
                        //adding the new data with old one available in Data Source of the List
                        fetching_from_server: false,
                        //updating the loading state to false
                    });
                })
                .catch(error => {
                    console.error(error);
                });
        });
    };
    renderHeader() {
        return (
            <View style={styles.footer}>

                <Text style={styles.btnText}>Movie App</Text>

            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                {this.state.loading ? (
                    <ActivityIndicator size="large" />
                ) : (
                        <FlatList
                            style={{ width: '100%' }}
                            keyExtractor={(item, index) => index.toString()}
                            data={this.state.serverData}
                            renderItem={({ item, index }) => (
                                <View style={styles.item}>
                                    <Text style={styles.text}>

                                        {item.name.toUpperCase()}
                                    </Text>
                                    {/* <MovieCard>{item.title}</MovieCard> */}
                                    <MovieList>{this.props.navigation}</MovieList>
                                </View>
                            )}
                            ItemSeparatorComponent={() => <View style={styles.separator} />}
                            ListHeaderComponent={this.renderHeader.bind(this)}

                        />
                    )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 30,
    },
    item: {
        padding: 10,
    },
    separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    loadMoreBtn: {
        padding: 10,
        backgroundColor: '#800000',
        borderRadius: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnText: {
        color: 'black',
        fontSize: 30,
        textAlign: 'center',
        fontStyle: 'italic',
        fontWeight: 'bold'
    },
});