import React from 'react'
import { StyleSheet, View, Dimensions, Image, StatusBar, TextInput, Alert, } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Text } from 'native-base';
import { api_key, imageURL } from '../Services/config';

const { width, height } = Dimensions.get('window')


//const contentInset = { top: 10, bottom: 10 }

const MovieCard = (props) => {
    var CardDatas = props.children;

    console.log("CardDatas/////////", CardDatas.poster_path)
    let uri = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/' + CardDatas.poster_path.toString();
    return (

        <View style={styles.item}>

            <Image source={{ uri: uri }} />
            <Text style={{ alignSelf: 'flex-end', }}>{CardDatas.title}</Text>


        </View>

    )


}

export default MovieCard

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 30,
    },
    item: {
        padding: 10,
        height: 150,
        width: 150,
        borderRadius: 2,
    },
    separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    text: {
        fontSize: 15,
        color: 'black',
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    loadMoreBtn: {
        padding: 10,
        backgroundColor: '#800000',
        borderRadius: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnText: {
        color: 'white',
        fontSize: 15,
        textAlign: 'center',
    },
});