//This is an example of React Native 
//FlatList Pagination to Load More Data dynamically - Infinite List
import React, { Component } from 'react';
//import react in our code.

import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    FlatList,
    Platform,
    ActivityIndicator,
    Image
} from 'react-native';
//import all the components we are going to use.
import MovieCard from './MovieCard'
import { api_key, imageURL } from '../Services/config';
import axios from '../../App/Services/axios'



export default class MovieList extends Component {



    constructor() {
        super();
        this.state = {
            loading: true,
            //Loading state used while loading the data for the first time
            serverData: [],
            //Data Source for the FlatList
            fetching_from_server: false,
            //Loading state used while loading more data
        };
        this.offset = 1;
        //Index of the offset to load from web API
    }

    componentDidMount() {

        this.getMovieData();
    }

    getMovieData = async () => {
        const url = 'genre/' + 28 + '/movies';
        axios
            .get(url, { params: { api_key } })
            .then(data => {
                this.setState({

                    serverData: data.data.results,
                    loading: false,
                });


            })
            .catch(error => {
                console.log('error', error.message);
            });
    }



    render() {
        return (
            <View style={styles.container}>
                {this.state.loading ? (
                    <ActivityIndicator size="large" />
                ) : (
                        <FlatList
                            horizontal={true}
                            style={{ width: '100%' }}

                            keyExtractor={(item, index) => index.toString()}
                            data={this.state.serverData}
                            renderItem={({ item, index }) => (

                                <TouchableOpacity onPress={() => this.props.children.navigate('DetailPage')}>

                                    <MovieCard>{item}</MovieCard>
                                </TouchableOpacity>

                            )}
                            ItemSeparatorComponent={() => <View style={styles.separator} />}

                        />
                    )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 30,
    },
    item: {
        padding: 10,
        height: 150,
        width: 150
    },
    separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    text: {
        fontSize: 15,
        color: 'black',
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    loadMoreBtn: {
        padding: 10,
        backgroundColor: '#800000',
        borderRadius: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnText: {
        color: 'white',
        fontSize: 15,
        textAlign: 'center',
    },
});