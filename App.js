//This is an example of React Native 
//FlatList Pagination to Load More Data dynamically - Infinite List
import React, { Component } from 'react';
//import react in our code.
import AppNavigation from './App/Navigation/AppNavigation'
import { NavigationContainer } from '@react-navigation/native';

export default class App extends Component {


  render() {
    return (
      <NavigationContainer>

        <AppNavigation />
      </NavigationContainer>

    );
  }
}

